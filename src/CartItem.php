<?php

namespace  KDA\Livewire\Cart;

use App\Mail\SiteContact as Notification;

use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;
use Livewire\Component;
use Mews\Captcha\Facades\Captcha;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Illuminate\Validation\ValidationException;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use KDA\Laravel\ShoppingCart\Facades\CartManager;

abstract class CartItem extends BaseComponent
{
   
    protected $listeners = [
        'recomputeQty' => 'updateQuantity',
    ];
    public $item;
    public $qty;

    protected $rules = [
        'qty'=>'required | numeric'
    ];

    protected $messages = [
        'qty'=>'Le champs quantité doit contenir un nombre'
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    
    public function mount($item){
        $this->item = $item;
        $this->qty = $item->qty;
    }

    public function add()
    {
        $this->item?->cart?->addItem($this->item);
        $this->qty = $this->item->refresh()->qty;
        $this->emit('productAdded');
    }

    public function remove()
    {

        if ($this->item) {
            $this->item?->cart?->removeItem($this->item);
            $this->qty = $this->item?->refresh()?->qty ??0;

        }
        $this->emit('productAdded');
    }
    
    public function updateQuantity(){
        $this->validate();
        $this->item->qty=$this->qty;
        $this->item->save();
        $this->render();
        $this->emit('productAdded');
    }
}
