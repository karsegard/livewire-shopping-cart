<?php

namespace  KDA\Livewire\Cart;

use Livewire\Component;

abstract class BaseComponent extends Component
{
    protected string $view;

    public function render()
    {
        return view($this->view,$this->getRenderData());
    }

    public function getRenderData(){
       return [];
    }

}
