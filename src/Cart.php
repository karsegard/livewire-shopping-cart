<?php

namespace  KDA\Livewire\Cart;

use KDA\Laravel\ShoppingCart\Facades\CartManager;

abstract class Cart extends BaseComponent
{
    public $showMessage = false;
    protected $listeners = [
        'refreshCart'=> '$refresh',
        'productAdded' => 'updateCart',
        'productRemoved' => 'updateCart',
        'clearCart' => 'updateCart'
    ];

    public $group ;
    public $label;
    public function mount($group='',$label=''){
        $this->group = $group;
        $this->label = $label;
    }

   
    protected function getCart(){
        return CartManager::create(false)->request(request())->group($this->group)->getCart();
    }
    public function getRenderData(){
        $cart =  CartManager::create(false)->request(request())->group($this->group)->getCart();
        $cart?->load('items');
        return [
            'cart'=>$cart
        ];
    }


    public function updateCart(){
      /*  $this->mount($this->group,$this->label);
        $this->render();
*/
       // $this->render();
    }

    public function recomputeCart(){
        $this->emit('recomputeQty');
    }
}
