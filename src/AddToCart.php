<?php

namespace KDA\Livewire\Cart;

use App\Mail\SiteContact as Notification;

use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;
use Livewire\Component;
use Mews\Captcha\Facades\Captcha;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Illuminate\Validation\ValidationException;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Facades\CartManager;

abstract class AddToCart extends Component
{
   
    
    public $group ;
   // public $product_class;
   // public $product_key;
    public $product; 
    public function mount(Model $product,$group=''){
        $this->group = $group;
        //$this->product_class=get_class($product);
        //$this->product_key=$product->getKey();
        
        $this->product = $product;
    }

    public function render()
    {
        $cart =  CartManager::create(false)->request(request())->group($this->group)->getCart();
        return view($this->view,['cart'=>$cart]);
    }

    public function addToCart(){

        CartManager::create(true)->request(request())->addItem($this->product,1);
        
        $this->emit('productAdded');
        return response();
    }
}
